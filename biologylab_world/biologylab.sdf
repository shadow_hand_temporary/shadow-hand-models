<?xml version="1.0"?>
<sdf version="1.5">
  <world name="default">

    <!-- lights -->
    <light name='sunlight' type='directional'>
      <cast_shadows>1</cast_shadows>
      <pose>-2.20195 -3.38232 2.27858 0 0 0</pose>
      <diffuse>1 1 1 1</diffuse>
      <specular>0.2 0.2 0.2 1</specular>
      <attenuation>
        <range>1000</range>
        <constant>0.9</constant>
        <linear>0.01</linear>
        <quadratic>0.001</quadratic>
      </attenuation>
      <direction>0.7 0.4 -0.5</direction>
    </light>

    <!-- ground and room environment -->
    <model name="ground_plane">
      <static>true</static>
      <link name="link">
        <collision name="collision">
          <geometry>
            <plane>
              <normal>0 0 1</normal>
              <size>10000 10000</size>
            </plane>
          </geometry>
          <surface>
            <friction>
              <ode>
                <mu>100</mu>
                <mu2>50</mu2>
              </ode>
            </friction>
            <contact>
              <ode/>
            </contact>
            <bounce/>
          </surface>
          <max_contacts>10</max_contacts>
        </collision>
        <self_collide>0</self_collide>
        <kinematic>0</kinematic>
        <gravity>1</gravity>
      </link>
    </model>
    <scene>
      <ambient>0.4 0.4 0.4 1</ambient>
      <background>0.7 0.7 0.7 1</background>
      <shadows>1</shadows>
    </scene>

    <model name="room">
      <static>true</static>
      <link name="floor">
        <pose>-0.522278 -0.501937 0.026121 3.14 0 3.14</pose>
        <collision name="collisionfloor">
          <geometry>
            <box>
              <size>5.05 3.95 0.01</size>
            </box>
          </geometry>
        </collision>
        <visual name="lab_tex_floor">
          <cast_shadows>0</cast_shadows>
          <geometry>
            <mesh>
              <uri>model://biologylab_world/meshes/labor_floor2.dae</uri>
            </mesh>
          </geometry>
        </visual>
        <self_collide>0</self_collide>
        <kinematic>0</kinematic>
        <gravity>1</gravity>
      </link>
      <link name="wall">
        <pose>-0.519806 -0.509678 1.299833 0 0 0</pose>
        <collision name="collisionwall_1">
          <pose>0 1.97 0 0 0 0</pose>
          <geometry>
            <box>
              <size>5.05 0.01 2.65</size>
            </box>
          </geometry>
        </collision>
        <collision name="collisionwall_2">
          <pose>0 -1.97 0 0 0 0</pose>
          <geometry>
            <box>
              <size>5.05 0.01 2.65</size>
            </box>
          </geometry>
        </collision>
        <collision name="collisionwall_3">
          <pose>2.525 0 0 0 0 0</pose>
          <geometry>
            <box>
              <size>0.01 3.95 2.65</size>
            </box>
          </geometry>
        </collision>
        <collision name="collisionwall_4">
          <pose>-2.525 0 0 0 0 0</pose>
          <geometry>
            <box>
              <size>0.01 3.95 2.65</size>
            </box>
          </geometry>
        </collision>
        <visual name="labtexwall">
          <cast_shadows>1</cast_shadows>
          <geometry>
            <mesh>ceil
              <uri>model://biologylab_world/meshes/lab_newroom.dae</uri>
            </mesh>
          </geometry>
        </visual>
        <self_collide>0</self_collide>
        <kinematic>0</kinematic>
        <gravity>1</gravity>
      </link>
      <link name="lights">
        <pose>-0.259984 -0.946394 2.368024 0 0 0</pose>
        <collision name="collisionlights">
          <pose>-0.25 0.25 0.075 0 0 0</pose>
          <geometry>
            <box>
              <size>3.9 3.1 0.25</size>
            </box>
          </geometry>
        </collision>
        <visual name="lights">
          <cast_shadows>0</cast_shadows>
          <geometry>
            <mesh>
              <uri>model://biologylab_world/meshes/lights.dae</uri>
            </mesh>
          </geometry>
        </visual>
        <self_collide>0</self_collide>
        <kinematic>0</kinematic>
        <gravity>1</gravity>
      </link>
    </model>

    <!-- chairs -->
    <model name="chairleft">
      <pose>0.564391 -1.188811 0.764238 0 0 -1.5</pose>
      <static>false</static>
      <link name="body">
        <collision name="collisionchair">
          <pose> 0 -0.075 -0.025 0 0 0</pose>
          <geometry>
            <box>
              <size>0.5 0.65 1.3</size>
            </box>
          </geometry>
        </collision>
        <visual name="chairleft">
          <cast_shadows>1</cast_shadows>
          <geometry>
            <mesh>
              <uri>model://biologylab_world/meshes/chair_left.dae</uri>
            </mesh>
          </geometry>
        </visual>
        <self_collide>0</self_collide>
        <kinematic>0</kinematic>
        <gravity>1</gravity>
      </link>
    </model>

    <model name="chairright">
      <pose>-2.413051 -1.274267 0.764238 0 0 0</pose>
      <static>false</static>
      <link name="body">
        <collision name="collisionchair2">
          <pose> 0 -0.075 -0.025 0 0 0</pose>
          <geometry>
            <box>
              <size>0.5 0.65 1.3</size>
            </box>
          </geometry>
        </collision>
        <visual name="chairright">
          <cast_shadows>1</cast_shadows>
          <geometry>
            <mesh>
              <uri>model://biologylab_world/meshes/chair_left.dae</uri>
            </mesh>
          </geometry>
        </visual>
        <self_collide>0</self_collide>
        <kinematic>0</kinematic>
        <gravity>1</gravity>
      </link>
    </model>

    <model name="chairmiddle">
      <pose>-1.036683 -0.748714 0.764238 0 0 0.8</pose>
      <static>false</static>
      <link name="body">
        <collision name="collisionchair3">
          <pose> 0.025 -0.05 -0.025 0 0 0.39</pose>
          <geometry>
            <box>
              <size>0.55 0.625 1.3</size>
            </box>
          </geometry>
        </collision>
        <visual name="chairmiddle">
          <cast_shadows>1</cast_shadows>
          <geometry>
            <mesh>
              <uri>model://biologylab_world/meshes/chair_left.dae</uri>
            </mesh>
          </geometry>
        </visual>
        <self_collide>0</self_collide>
        <kinematic>0</kinematic>
        <gravity>1</gravity>
      </link>
    </model>

    <!-- cabinets and shelves -->
    <model name="anlage">
      <pose>-0.829635 -2.352034 1.327868 0 0 0</pose>
      <static>true</static>
      <link name="body">
        <collision name="base">
          <pose>-0.025 0.3 -0.75 0 0 0</pose>
          <geometry>
            <box>
              <size>4.325 0.9 1.1</size>
            </box>
          </geometry>
        </collision>
        <collision name="back">
          <pose>-0.025 -0.08 0.05 0 0 0</pose>
          <geometry>
            <box>
              <size>4.325 0.125 0.5</size>
            </box>
          </geometry>
        </collision>
        <collision name="faucet_1">
          <pose>-1.335 0.075 0.025 0 0 0</pose>
          <geometry>
            <box>
              <size>0.3 0.215 0.125</size>
            </box>
          </geometry>
        </collision>
        <collision name="faucet_2">
          <pose>-0.2875 0.075 0.025 0 0 0</pose>
          <geometry>
            <box>
              <size>0.3 0.215 0.125</size>
            </box>
          </geometry>
        </collision>
        <collision name="faucet_3">
          <pose>0.85 0.075 0.025 0 0 0</pose>
          <geometry>
            <box>
              <size>0.3 0.215 0.125</size>
            </box>
          </geometry>
        </collision>
        <collision name="faucet_4">
          <pose>1.9075 0.075 0.025 0 0 0</pose>
          <geometry>
            <box>
              <size>0.3 0.215 0.125</size>
            </box>
          </geometry>
        </collision>
        <visual name="die anlage">
          <cast_shadows>1</cast_shadows>
          <geometry>
            <mesh>
              <uri>model://biologylab_world/meshes/lab_anlage.dae</uri>
            </mesh>
          </geometry>
        </visual>
        <self_collide>0</self_collide>
        <kinematic>0</kinematic>
        <gravity>1</gravity>
      </link>
    </model>

    <model name="labanlage">
      <static>true</static>
      <link name="labanlageP1">
        <pose>0.533081 1.235013 1.547591 0 0 0</pose>
        <collision name="collisionlabanlage1">
          <pose>0.0 0.02 0.1 0 0 0.0</pose>
          <geometry>
            <box>
              <size>2.0 0.19 1.1</size>
            </box>
          </geometry>
        </collision>
        <collision name="collisionlabanlage1_faucet">
          <pose>0.65 -0.15 0.05 0 0 0.0</pose>
          <geometry>
            <box>
              <size>0.1 0.175 0.175</size>
            </box>
          </geometry>
        </collision>
        <visual name="anlage1">P3
          <cast_shadows>1</cast_shadows>
          <geometry>
            <mesh>
              <uri>model://biologylab_world/meshes/spuleAnlageP1.dae</uri>
            </mesh>
          </geometry>
        </visual>
        <self_collide>0</self_collide>
        <kinematic>0</kinematic>
        <gravity>1</gravity>
      </link>
      <link name="labanlageP2">
        <pose>0.806265 0.691988 1.161095 0 0 0</pose>
        <visual name="anlage2">
          <cast_shadows>1</cast_shadows>
          <geometry>
            <mesh>
              <uri>model://biologylab_world/meshes/spuleAnlageP2.dae</uri>
            </mesh>
          </geometry>
        </visual>
        <self_collide>0</self_collide>
        <kinematic>0</kinematic>
        <gravity>1</gravity>
      </link>
      <link name="labanlage3">
        <pose>0.742711 0.664493 0.777885 0 0 0</pose>
        <collision name="collisionlabanlage3">
          <pose>-0.1 0.125 -0.175 0 0 0.0</pose>
          <geometry>
            <box>
              <size>2.85 1.25 1.20</size>
            </box>
          </geometry>
        </collision>
        <visual name="anlage3">
          <cast_shadows>1</cast_shadows>
          <geometry>
            <mesh>
              <uri>model://biologylab_world/meshes/spuleAnlageP3.dae</uri>
            </mesh>
          </geometry>
        </visual>
        <self_collide>0</self_collide>
        <kinematic>0</kinematic>
        <gravity>1</gravity>
      </link>
    </model>


    <model name="shelf">
      <pose>-1.2 1.368933 1.368933 0 0 0</pose>
      <static>true</static>
      <link name="body">
        <collision name="collisionlabshelf">
          <pose>0 -0.05 -0.025 0 0 0</pose>
          <geometry>
            <box>
              <size>1.25 0.215 0.225</size>
            </box>
          </geometry>
        </collision>
        <visual name="lab_shelf">
          <cast_shadows>1</cast_shadows>
          <geometry>
            <mesh>
              <uri>model://biologylab_world/meshes/labor_shelf.dae</uri>
            </mesh>
          </geometry>
        </visual>
        <self_collide>0</self_collide>
        <kinematic>0</kinematic>
        <gravity>1</gravity>
      </link>
    </model>

    <model name="trashcan">
      <pose>-1.2 1.047361 0.382010 0 0 0</pose>
      <static>false</static>
      <link name="body">
        <collision name="collisiontrashcan">
          <pose>0 0 -0.05 0 0 0</pose>
          <geometry>
            <box>
              <size>0.5 0.625 0.625</size>
            </box>
          </geometry>
        </collision>
        <visual name="trashcan">
          <cast_shadows>1</cast_shadows>
          <geometry>
            <mesh>
              <uri>model://biologylab_world/meshes/trashcan.dae</uri>
            </mesh>
          </geometry>
        </visual>
        <self_collide>0</self_collide>
        <kinematic>0</kinematic>
        <gravity>1</gravity>
      </link>
    </model>

    <!-- tripots, 3 on main counter and 1 on shelf -->
    <model name="tripot">
      <pose>-2.699128 -2.183371 1.269882 0 0 0.7888</pose>
      <static>false</static>
      <link name="body">
        <collision name="collisiontripot">
          <pose>0.0 -0.0125 -0.04 0 0 0</pose>
          <geometry>
            <cylinder>
              <radius>0.12</radius>
              <length>0.22</length>
            </cylinder>
          </geometry>
        </collision>
        <visual name="tripot">
          <cast_shadows>1</cast_shadows>
          <geometry>
            <mesh>
              <uri>model://biologylab_world/meshes/tripot.dae</uri>
            </mesh>
          </geometry>
        </visual>

        <self_collide>0</self_collide>
        <kinematic>0</kinematic>
        <gravity>1</gravity>
      </link>
    </model>

    <model name="tripot2">
      <pose>-1.713335 -2.149267 1.269882 0 0 -1.990</pose>
      <static>false</static>
      <link name="body">
        <collision name="collisiontripot2">
          <pose>0.0 -0.0125 -0.04 0 0 0</pose>
          <geometry>
            <cylinder>
              <radius>0.12</radius>
              <length>0.22</length>
            </cylinder>
          </geometry>
        </collision>
        <visual name="tripot2">
          <cast_shadows>1</cast_shadows>
          <geometry>
            <mesh>
              <uri>model://biologylab_world/meshes/tripot.dae</uri>
            </mesh>
          </geometry>
        </visual>
        <self_collide>0</self_collide>
        <kinematic>0</kinematic>
        <gravity>1</gravity>
      </link>
    </model>

    <model name="tripot3">
      <pose>0.132048 -2.037078 1.269882 0 0 0</pose>
      <static>false</static>
      <link name="body">
        <collision name="collisiontripot3">
          <pose>0.0 -0.0125 -0.04 0 0 0</pose>
          <geometry>
            <cylinder>
              <radius>0.12</radius>
              <length>0.22</length>
            </cylinder>
          </geometry>
        </collision>
        <visual name="tripot3">
          <cast_shadows>1</cast_shadows>
          <geometry>
            <mesh>
              <uri>model://biologylab_world/meshes/tripot.dae</uri>
            </mesh>
          </geometry>
        </visual>
        <self_collide>0</self_collide>
        <kinematic>0</kinematic>
        <gravity>1</gravity>
      </link>
    </model>

    <!-- water bottles, 3 on main counter and 1 on shelf -->
    <model name="water_bottle">
      <pose>-1.859208 -1.788774 1.330170 0 0 0</pose>
      <static>false</static>
      <link name="body">
        <collision name="collisionwaterbottle">
          <pose>0.0125 0 -0.105 0 0 0</pose>
          <geometry>
            <cylinder>
              <radius>0.0425</radius>
              <length>0.205</length>
            </cylinder>
          </geometry>
        </collision>
        <collision name="collisionwaterbottle_straw">
          <pose>-0.025 0 0.07 0 0 0</pose>
          <geometry>
            <box>
              <size>0.125 0.01 0.1525</size>
            </box>
          </geometry>
        </collision>
        <visual name="water_bottle">
          <cast_shadows>1</cast_shadows>
          <geometry>
            <mesh>
              <uri>model://biologylab_world/meshes/water_bottle.dae</uri>
            </mesh>
          </geometry>
        </visual>
        <self_collide>0</self_collide>
        <kinematic>0</kinematic>
        <gravity>1</gravity>
      </link>
    </model>

    <model name="water_bottle2">
      <pose>-2.470309 -2.086330 1.330170 0 0 0</pose>
      <static>false</static>
      <link name="body">
        <collision name="collisionwaterbottle2">
          <pose>0.0125 0 -0.105 0 0 0</pose>
          <geometry>
            <cylinder>
              <radius>0.0425</radius>
              <length>0.205</length>
            </cylinder>
          </geometry>
        </collision>
        <collision name="collisionwaterbottle2_straw">
          <pose>-0.025 0 0.07 0 0 0</pose>
          <geometry>
            <box>
              <size>0.125 0.01 0.1525</size>
            </box>
          </geometry>
        </collision>
        <visual name="water_bottle2">
          <cast_shadows>1</cast_shadows>
          <geometry>
            <mesh>
              <uri>model://biologylab_world/meshes/water_bottle.dae</uri>
            </mesh>
          </geometry>
        </visual>
        <self_collide>0</self_collide>
        <kinematic>0</kinematic>
        <gravity>1</gravity>
      </link>
    </model>

    <model name="water_bottle_laying">
      <pose>0.544761 -1.896440 1.173303 -1.600872 -0.108702 -1.567532</pose>
      <static>false</static>
      <link name="body">
        <collision name="collisionwaterbottlelaying">
          <pose>0.0125 0 -0.105 0 0 0</pose>
          <geometry>
            <cylinder>
              <radius>0.0425</radius>
              <length>0.205</length>
            </cylinder>
          </geometry>
        </collision>
        <collision name="collisionwaterbottlelaying_straw">
          <pose>-0.025 0 0.07 0 0 0</pose>
          <geometry>
            <box>
              <size>0.125 0.01 0.1525</size>
            </box>
          </geometry>
        </collision>
        <visual name="water_bottle_laying">
          <cast_shadows>1</cast_shadows>
          <geometry>
            <mesh>
              <uri>model://biologylab_world/meshes/water_bottle.dae</uri>
            </mesh>
          </geometry>
        </visual>
        <self_collide>0</self_collide>
        <kinematic>0</kinematic>
        <gravity>1</gravity>
      </link>
    </model>

    <model name="water_bottle_shelf">
      <pose>-1.363850 1.312548 1.666765 0 0 1.500000</pose>
      <static>false</static>
      <link name="body">
        <collision name="collision">
          <pose>0.0125 0 -0.105 0 0 0</pose>
          <geometry>
            <cylinder>
              <radius>0.0425</radius>
              <length>0.205</length>
            </cylinder>
          </geometry>
        </collision>
        <collision name="collisionwaterbottleshelf_straw">
          <pose>-0.025 0 0.07 0 0 0</pose>
          <geometry>
            <box>
              <size>0.125 0.01 0.1525</size>
            </box>
          </geometry>
        </collision>
        <visual name="water_bottle_shelf">
          <cast_shadows>1</cast_shadows>
          <geometry>
            <mesh>
              <uri>model://biologylab_world/meshes/water_bottle.dae</uri>
            </mesh>
          </geometry>
        </visual>
        <self_collide>0</self_collide>
        <kinematic>0</kinematic>
        <gravity>1</gravity>
      </link>
    </model>

    <!-- ymaze and two red/blue display screens -->
    <model name="ymaze">
      <pose>-0.855040 -1.996673 1.162897 0 0 0</pose>
      <static>true</static>
      <link name="body">
        <collision name="base">
          <pose>-0.055 0 -0.04 0 0 0</pose>
          <geometry>
            <box>
              <size>1.23 0.61 0.01</size>
            </box>
          </geometry>
        </collision>
        <collision name="wall_r">
          <pose>-0.31 -0.0525 0.0025 0 0 0</pose>
          <geometry>
            <box>
              <size>0.485 0.005 0.075</size>
            </box>
          </geometry>
        </collision>
        <collision name="wall_l">
          <pose>-0.31 0.0525 0.0025 0 0 0</pose>
          <geometry>
            <box>
              <size>0.485 0.005 0.075</size>
            </box>
          </geometry>
        </collision>
        <collision name="wall_rear">
          <pose>-0.545 0 0.0025 0 0 0</pose>
          <geometry>
            <box>
              <size>0.0075 0.11 0.075</size>
            </box>
          </geometry>
        </collision>
        <collision name="wall_r_diag_outer">
          <pose>0.14 -0.1325 0.0025 0 0 -0.3665</pose>
          <geometry>
            <box>
              <size>0.45 0.005 0.075</size>
            </box>
          </geometry>
        </collision>
        <collision name="wall_r_diag_inner">
          <pose>0.215 -0.06 0.0025 0 0 -0.3665</pose>
          <geometry>
            <box>
              <size>0.35 0.005 0.075</size>
            </box>
          </geometry>
        </collision>
        <collision name="wall_r_diag_end">
          <pose>0.365 -0.1675 0.0025 0 0 -0.3491</pose>
          <geometry>
            <box>
              <size>0.0075 0.10 0.075</size>
            </box>
          </geometry>
        </collision>
        <collision name="wall_l_diag_outer">
          <pose>0.14 0.1325 0.0025 0 0 0.3665</pose>
          <geometry>
            <box>
              <size>0.45 0.005 0.075</size>
            </box>
          </geometry>
        </collision>
        <collision name="wall_l_diag_inner">
          <pose>0.215 0.06 0.0025 0 0 0.3665</pose>
          <geometry>
            <box>
              <size>0.35 0.005 0.075</size>
            </box>
          </geometry>
        </collision>
        <collision name="wall_l_diag_end">
          <pose>0.365 0.1675 0.0025 0 0 0.3491</pose>
          <geometry>
            <box>
              <size>0.0075 0.10 0.075</size>
            </box>
          </geometry>
        </collision>
        <visual name="ymaze">
          <cast_shadows>1</cast_shadows>
          <geometry>
            <mesh>
              <uri>model://biologylab_world/meshes/ymaze.dae</uri>
            </mesh>
          </geometry>
        </visual>
        <self_collide>0</self_collide>
        <kinematic>0</kinematic>
        <gravity>1</gravity>
      </link>
    </model>

    <model name="right_vr_screen">
      <pose>-0.504680 -2.158381 1.13129 0 0 -1.951584</pose>
      <static>true</static>
      <link name="body">
        <visual name="screen">
          <cast_shadows>1</cast_shadows>
          <geometry>
            <mesh>
              <uri>model://vr_screen/meshes/screen_big.dae</uri>
              <scale>0.025 0.025 0.025</scale>
            </mesh>
          </geometry>
        </visual>
        <visual name="screen_glass">
          <cast_shadows>0</cast_shadows>
          <pose>0.000 0.001 0.035 0 -0 0</pose>
          <geometry>
            <box>
              <size>0.0827 0.00034 0.05488235294</size>
            </box>
          </geometry>
          <material>
            <script>
              <uri>file://media/materials/scripts/gazebo.material</uri>
              <name>Gazebo/BlueGlow</name>
            </script>
          </material>
        </visual>
        <self_collide>0</self_collide>
        <kinematic>0</kinematic>
        <gravity>1</gravity>
      </link>
    </model>

    <model name="left_vr_screen">
      <pose>-0.512436 -1.836369 1.132169 0 0 -1.231590</pose>
      <static>true</static>
      <link name="body">
        <visual name="screen">
          <cast_shadows>1</cast_shadows>
          <geometry>
            <mesh>
              <uri>model://vr_screen/meshes/screen_big.dae</uri>
              <scale>0.025 0.025 0.025</scale>
            </mesh>
          </geometry>
        </visual>
        <visual name="screen_glass">
          <cast_shadows>0</cast_shadows>
          <pose>0.000 0.001 0.035 0 -0 0</pose>
          <geometry>
            <box>
              <size>0.0827 0.00034 0.05488235294</size>
            </box>
          </geometry>
          <material>
            <script>
              <uri>file://media/materials/scripts/gazebo.material</uri>
              <name>Gazebo/BlueGlow</name>
            </script>
          </material>
        </visual>
        <self_collide>0</self_collide>
        <kinematic>0</kinematic>
        <gravity>1</gravity>
      </link>
    </model>

    <model name='nrpposter'>
      <static>true</static>
      <pose>2.03767 -2.0524 1.79971 0 0 -3.1415</pose>
      <include>
          <uri>model://viz_postersmall_2</uri>
      </include>
    </model>


  </world>
</sdf>
