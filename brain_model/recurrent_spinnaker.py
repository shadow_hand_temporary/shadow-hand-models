# -*- coding: utf-8 -*-
"""
This file contains a recurrent network
"""
# pragma: no cover

__author__ = 'Georg Hinkel'

from hbp_nrp_cle.brainsim import simulator as sim

left = sim.Population(5, sim.IF_curr_exp(), label="left")
right = sim.Population(5, sim.IF_curr_exp(), label="right")

self_connect = sim.StaticSynapse(weight=0.7, delay=sim.RandomDistribution('uniform', (1.0, 10.0)))
all_connector = sim.AllToAllConnector()
sim.Projection(
    presynaptic_population=left, postsynaptic_population=left,
    connector=all_connector, synapse_type=self_connect, receptor_type="excitatory"
)
sim.Projection(
    presynaptic_population=right, postsynaptic_population=right,
    connector=all_connector, synapse_type=self_connect, receptor_type="excitatory"
)