#!/bin/bash
# Author: Bernd Eckstein, Luc Guyot
#
# This script find all robot and environment models
# based on model.config files.
# It creates symlinks both  in ~/.gazebo/models (for Gazebo)
# and ${HBP}/gzweb/http/client/assets (for the web).
# It flattens models located in subdirectories

function log() {
    echo "[HBP-NRP] $(date --rfc-3339=seconds) $*"
}


function create_symlinks_for_models() {
    echo
    echo "-----------------------------------------------------------------------------------------------------"
    echo "  Creating symlinks to ~/.gazebo/models and to ${HBP}/gzweb/http/client/assets"
    echo "-----------------------------------------------------------------------------------------------------"
    echo
    echo "  Former links are overwritten!"
    echo
    local DIR=$(cd "$(dirname "${BASH_SOURCE[0]}")/$1" && pwd)
    # Filter out irrelevant subfolders (TODO(Luc): divide Models into sensible subfolders)
    exclude_dirs=(
        robots
        environments
        brain_model
        brains
        brain_visualizer
        integration_tests
    )
    MODELS=${NRP_MODELS_DIRECTORY}
    for dir in ${exclude_dirs[@]}; do
      exclude_paths="${exclude_paths} -not -path '${MODELS}/${dir}/*'"
    done

    # Find all model.config files and create symlinks
    model_paths=`find ${DIR} -name "model.config" ${exclude_paths}`
    for model in ${model_paths}; do
        model_dir=$(dirname ${model})
        model_basename=$(basename ${model_dir})
        gazebo_linkname=~/.gazebo/models/${model_basename}
        gzweb_linkname=${HBP}/gzweb/http/client/assets/${model_basename}
        log "Creating symlinks for ${model_dir}"
        rm -rf ${gazebo_linkname} ${gzweb_linkname}
        ln -s ${model_dir} ${gazebo_linkname}
        ln -s ${model_dir} ${gzweb_linkname}
    done

    for asset in libraries sky; do
        model_dir=${NRP_MODELS_DIRECTORY}/${asset}
        log "Creating a symlink for ${model_dir}"
        gzweb_linkname=${HBP}/gzweb/http/client/assets/${asset}
        rm -rf ${gzweb_linkname}
        ln -s ${model_dir} ${gzweb_linkname}
    done;
}


usage()
{
cat << EOF
Without options, this will create symlinks for models located in $HBP/Models
OPTIONS:
   -h      Show this message
   -d      Create symlinks for models located in the specified directory
EOF
exit
}

opt=$1
shift
case ${opt} in
    -d)
        echo "Create symlinks for every model folder located in $1"
        echo
        ;;
    "")
        echo "Create symlinks for every model folder located in ${HBP}/Models"
        echo
        ;;
    *)
       usage
       ;;
esac

create_symlinks_for_models $1

